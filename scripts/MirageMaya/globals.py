from maya import cmds, mel


def create_render_globals_node():
    if cmds.objExists("defaultMirageRenderGlobals"):
        return

    cmds.createNode(
        "MirageRendererGlobalsNode",
        name = "defaultMirageRenderGlobals",
        shared = True,
        skipSelect = True)


def delete_render_globals_node():
    if cmds.objExists("defaultMirageRenderGlobals"):
        cmds.delete("defaultMirageRenderGlobals")


def create_render_globals_tab():
    create_render_globals_node()

    parent_form = cmds.setParent(query=True)
    cmds.setUITemplate("renderGlobalsTemplate", pushTemplate=True)
    cmds.setUITemplate("attributeEditorTemplate", pushTemplate=True)

    column_width = 400

    cmds.scrollLayout("mirageRendererScrollLayout", horizontalScrollBarThickness=0)
    cmds.columnLayout("mirageRendererColumnLayout", adjustableColumn=True, width=column_width, rowSpacing=2)

    # = Mode =

    cmds.frameLayout("modeFrameLayout", label="Mode", collapsable=True, collapse=False)

    cmds.separator(height=2)

    cmds.attrEnumOptionMenuGrp(
        "mirageRenderType", 
        label = "Type", 
        columnWidth = (3, 160), 
        columnAttach= (1, "left", 4), 
        attribute = "defaultMirageRenderGlobals.renderType")

    cmds.attrEnumOptionMenuGrp(
        "mirageRenderMode",
        label = "Mode",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        attribute = "defaultMirageRenderGlobals.renderMode")

    cmds.attrEnumOptionMenuGrp(
        "mirageRenderFilterType",
        label = "Filter type",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        attribute = "defaultMirageRenderGlobals.filterType")

    cmds.setParent("..")

    # = Sampling =

    cmds.frameLayout("samplingFrameLayout", label="Sampling", collapsable=True, collapse=False)
                        
    cmds.separator(height=2)

    cmds.attrFieldSliderGrp(
        "mirageRenderExposure",
        label = "Exposure",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        minValue = 0,
        maxValue = 10,
        attribute = "defaultMirageRenderGlobals.exposure")

    cmds.attrFieldSliderGrp(
        "mirageRenderLimit",
        label = "Limit",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        minValue = 0,
        maxValue = 10,
        attribute = "defaultMirageRenderGlobals.limit")

    cmds.attrFieldSliderGrp(
        "mirageRenderSamples",
        label = "Samples",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        minValue = 1,
        maxValue = 100,
        attribute = "defaultMirageRenderGlobals.samples")

    cmds.setParent("..")
                        
    # = NLM Filtering =

    cmds.frameLayout("nlmFrameLayout", label="NLM", collapsable=True, collapse=False)

    cmds.separator(height=2)

    cmds.attrFieldSliderGrp(
        "mirageRenderNLMWidth",
        label = "Width",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        minValue = 1,
        attribute = "defaultMirageRenderGlobals.nlmWidth")

    cmds.attrFieldSliderGrp(
        "mirageRenderNLMFalloff",
        label = "Falloff",
        columnWidth = (3, 160),
        columnAttach= (1, "left", 4),
        minValue = 0,
        attribute = "defaultMirageRenderGlobals.nlmFalloff")

    cmds.setUITemplate("renderGlobalsTemplate", popTemplate=True)
    cmds.setUITemplate("attributeEditorTemplate", popTemplate=True)
    cmds.formLayout(
        parent_form,
        edit=True,
        attachForm=[
            ("MirageRendererScrollLayout", "top", 0),
            ("MirageRendererScrollLayout", "bottom", 0),
            ("MirageRendererScrollLayout", "left", 0),
            ("MirageRendererScrollLayout", "right", 0)])


def update_render_globals_tab():
    # just a placeholder
    return


# Register create Bounce Renderer Tab
mel.eval("""
global proc createMirageRenderGlobalsTab() {
    python("import MirageMaya; MirageMaya.create_render_globals_tab()");
}""")

# Register update Bounce Renderer Tab
mel.eval("""
global proc updateMirageRenderGlobalsTab() {
    python("import MirageMaya; MirageMaya.update_render_globals_tab()");
}""")
