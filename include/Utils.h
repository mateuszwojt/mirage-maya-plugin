#pragma once

#include <maya/MStatus.h>
#include <maya/MString.h>
#include <maya/MObject.h>
#include <maya/MSelectionList.h>

MStatus getDependencyNodeByName(const MString &name, MObject &node)
{
	MSelectionList selList;
	selList.add(name);

	if (selList.isEmpty())
		return MS::kFailure;

	return selList.getDependNode(0, node);
}