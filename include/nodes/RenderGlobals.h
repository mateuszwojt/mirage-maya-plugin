#pragma once

#include <maya/MPxNode.h>

#include <core/Renderer.h>

class RenderGlobalsNode : public MPxNode
{
public:
	static const MString name;
	static const MTypeId id;

	static void *creator();
	static MStatus initialize();

	MStatus compute(const MPlug &plug, MDataBlock &dataBlock) override;

	static void clean();

	static Mirage::Options getRenderOptions();

private:
	static MObject gRenderType;
	static MObject gRenderMode;
	static MObject gFilterType;

	static MObject gExposure;
    static MObject gLimit;
    static MObject gNumSamples;
    static MObject gEnableDOF;
	
	static MObject gNLMWidth;
    static MObject gNLMFalloff;
};