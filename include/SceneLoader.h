#include "adapters/Mesh.h"

#include <iostream>
#include <utility>
#include <functional>
#include <map>

#include <maya/MItDag.h>
#include <maya/MDagPath.h>
#include <maya/MString.h>

class SceneLoader {
public:
    SceneLoader() {
        registerAdapter(MFn::kMesh, new MeshAdapter());
    }

    MStatus load(Mirage::Scene &scene);
    bool registerAdapter(const MFn::Type &type, Adapter *adapter);

private:
    std::map<const MFn::Type, Adapter*> adapters;
};