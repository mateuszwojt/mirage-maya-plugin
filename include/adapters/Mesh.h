#include "Adapter.h"

#include <maya/MFnMesh.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MDagPath.h>

class MeshAdapter : public Adapter {
public:
    MeshAdapter() : name("MeshAdapter") {}

    MStatus load(MItDag &iter, Mirage::Scene &scene) override;

    const std::string name;
};