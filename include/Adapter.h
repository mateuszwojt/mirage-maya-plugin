#include <maya/MItDag.h>
#include <maya/MStatus.h>

#include <core/Scene.h>

class Adapter {
public:
    Adapter() = default;

    virtual MStatus load(MItDag &iter, Mirage::Scene &scene) = 0;

    const std::string name = "Adapter";
};