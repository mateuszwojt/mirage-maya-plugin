#pragma once

#include <maya/MPxCommand.h>

#include <core/Scene.h>
#include <core/Renderer.h>

class RenderProcedure : public MPxCommand
{
public:
	static const char *name;
	static void *creator();
	static MSyntax createSyntax();

	~RenderProcedure() override = default;

	MStatus doIt(const MArgList &arg) override;

	bool isUndoable() const override { return false; }
	bool hasSyntax() const override { return true; }

	void buildScene(MString cameraName);
	void translateCamera(MString cameraName);
	void translateLights();
	void translateGeo();

	void preRender();
	void initRender(MString camera);
	void render();
	void postRender();

private:
	Mirage::Scene		m_scene;
    Mirage::Options     m_renderOptions;
    Mirage::Renderer*   m_pRenderer;
    Mirage::Camera      m_Camera;
    Mirage::Color*      m_pPixels;
};