#include "nodes/RenderGlobals.h"
#include "Utils.h"

#include <cmath>

#include <maya/MDGModifier.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>

const MString RenderGlobalsNode::name("MirageRendererGlobalsNode");
const MTypeId RenderGlobalsNode::id(0x3ffff);

MObject RenderGlobalsNode::gRenderType;
MObject RenderGlobalsNode::gRenderMode;
MObject RenderGlobalsNode::gFilterType;

MObject RenderGlobalsNode::gExposure;
MObject RenderGlobalsNode::gLimit;
MObject RenderGlobalsNode::gNumSamples;
MObject RenderGlobalsNode::gEnableDOF;

MObject RenderGlobalsNode::gNLMWidth;
MObject RenderGlobalsNode::gNLMFalloff;

void *RenderGlobalsNode::creator()
{
	return (new RenderGlobalsNode);
}

MStatus RenderGlobalsNode::initialize()
{
	MStatus status;
	MFnNumericAttribute numAttr;
    MFnEnumAttribute eAttr;

	Mirage::Options opts;

	gRenderType = eAttr.create("renderType", "renderType", opts.type, &status);
	CHECK_MSTATUS(status);
    eAttr.addField("CPU", Mirage::RenderType::eCpu);
    eAttr.addField("GPU", Mirage::RenderType::eGpu);
	addAttribute(gRenderType);

	gRenderMode = eAttr.create("renderMode", "renderMode", opts.mode, &status);
	CHECK_MSTATUS(status);
    eAttr.addField("Normals", Mirage::RenderMode::eNormals);
    eAttr.addField("Complexity", Mirage::RenderMode::eComplexity);
    eAttr.addField("Path Trace", Mirage::RenderMode::ePathTrace);
	addAttribute(gRenderMode);

	gFilterType = eAttr.create("filterType", "filterType", opts.filter.type, &status);
	CHECK_MSTATUS(status);
    eAttr.addField("Box", Mirage::FilterType::eFilterBox);
    eAttr.addField("Gaussian", Mirage::FilterType::eFilterGaussian);
	addAttribute(gFilterType);

	gExposure = numAttr.create("exposure", "exp", MFnNumericData::kFloat, opts.exposure, &status);
	CHECK_MSTATUS(status);
	numAttr.setMin(0.f);
	numAttr.setMax(INFINITY);
	addAttribute(gExposure);

	gLimit = numAttr.create("limit", "lim", MFnNumericData::kFloat, opts.limit, &status);
	CHECK_MSTATUS(status);
	numAttr.setMin(0.f);
	numAttr.setMax(INFINITY);
	addAttribute(gLimit);

	gNumSamples = numAttr.create("numSamples", "samples", MFnNumericData::kInt, opts.maxSamples, &status);
	CHECK_MSTATUS(status);
	numAttr.setMin(0);
	numAttr.setMax(16);
	addAttribute(gNumSamples);

	gEnableDOF = numAttr.create("enableDof", "dof", MFnNumericData::kBoolean, opts.enableDOF, &status);
	CHECK_MSTATUS(status);
	addAttribute(gEnableDOF);

	gNLMWidth = numAttr.create("nlmWidth", "nlmWidth", MFnNumericData::kFloat, opts.nlmWidth, &status);
	CHECK_MSTATUS(status);
	numAttr.setMin(0);
	addAttribute(gNLMWidth);

	gNLMFalloff = numAttr.create("nlmFalloff", "nlmFalloff", MFnNumericData::kFloat, opts.nlmFalloff, &status);
	CHECK_MSTATUS(status);
	numAttr.setMin(0);
	addAttribute(gNLMFalloff);

	return (MS::kSuccess);
}

MStatus RenderGlobalsNode::compute(const MPlug &plug, MDataBlock &data)
{
	return (MS::kSuccess);
}

void RenderGlobalsNode::clean()
{
	MObject mObj;
	MDGModifier modifier;

	getDependencyNodeByName(RenderGlobalsNode::name, mObj);

	modifier.deleteNode(gRenderType);
	modifier.deleteNode(gRenderMode);
	modifier.deleteNode(gFilterType);
	modifier.doIt();

	modifier.deleteNode(gExposure);
	modifier.deleteNode(gLimit);
	modifier.deleteNode(gNumSamples);
	modifier.deleteNode(gEnableDOF);
	modifier.doIt();

	modifier.deleteNode(gNLMWidth);
	modifier.deleteNode(gNLMFalloff);
	modifier.doIt();
}

Mirage::Options RenderGlobalsNode::getRenderOptions()
{
	MObject mObj;
	Mirage::Options opts;

	if (getDependencyNodeByName(RenderGlobalsNode::name, mObj) != MS::kSuccess)
	{
		return Mirage::Options();
	}
	
	int renderType;
	MPlug pRenderType(mObj, gRenderType);
	pRenderType.getValue(renderType);
	opts.type = static_cast<Mirage::RenderType>(renderType);

	int renderMode;
	MPlug pRenderMode(mObj, gRenderMode);
	pRenderMode.getValue(renderMode);
	opts.mode = static_cast<Mirage::RenderMode>(renderMode);

	int filterType;
	MPlug pFilterType(mObj, gFilterType);
	pFilterType.getValue(filterType);
	opts.filter = Mirage::Filter(static_cast<Mirage::FilterType>(filterType), 0.75f, 1.0f);

	MPlug pExposure(mObj, gExposure);
	pExposure.getValue(opts.exposure);

	MPlug pLimit(mObj, gLimit);
	pLimit.getValue(opts.limit);

	MPlug pNumSamples(mObj, gNumSamples);
	pNumSamples.getValue(opts.maxSamples);

	MPlug pEnableDOF(mObj, gEnableDOF);
	pEnableDOF.getValue(opts.enableDOF);

	MPlug pNLMWidth(mObj, gNLMWidth);
	pNLMWidth.getValue(opts.nlmWidth);

	MPlug pNLMFalloff(mObj, gNLMFalloff);
	pNLMFalloff.getValue(opts.nlmFalloff);
	
	return opts;
}
