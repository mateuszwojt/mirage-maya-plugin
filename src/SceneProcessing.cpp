#include "RenderProcedure.h"

#include <utils/Util.h>

#include <maya/MDagPath.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MIntArray.h>
#include <maya/MMatrix.h>
#include <maya/MQuaternion.h>
#include <maya/MPointArray.h>
#include <maya/MSelectionList.h>
#include <maya/MFnCamera.h>
#include <maya/MFnTransform.h>
#include <maya/MItDag.h>
#include <maya/MFnMesh.h>

void RenderProcedure::translateCamera(MString cameraName)
{
	std::cout << "Starting translateCamera() procedure" << std::endl;
	MDagPath mDag;
	MSelectionList mList;
	mList.add(cameraName);
	mList.getDagPath(0, mDag);
	MFnCamera camera(mDag);
	MPoint mOrigin = camera.eyePoint(MSpace::kWorld);
	MPoint mInterestPoint = camera.centerOfInterestPoint(MSpace::kWorld);

	// set camera position and rotation
	m_Camera.position = Mirage::Vec3(mOrigin.x, mOrigin.y, mOrigin.z);
	std::cout << "\tCamera position : " << m_Camera.position.x << ", " << m_Camera.position.y << ", " << m_Camera.position.z << std::endl;

	MFnTransform mXform(mDag);
	MQuaternion rotation;
	mXform.getRotation(rotation, MSpace::kWorld);
	m_Camera.rotation = Mirage::Quat(rotation.x, rotation.y, rotation.z, rotation.w);
	std::cout << "\tCamera rotation : " << m_Camera.rotation.x << ", " << m_Camera.rotation.y << ", " << m_Camera.rotation.z << ", " << m_Camera.rotation.w << std::endl;

	// set camera FOV
	double focalLength = camera.focalLength();
	std::cout << "\tCamera focal length : " << focalLength << std::endl;
	double horizAperture = camera.horizontalFilmAperture() * 25.4f;
	std::cout << "\tCamera horizontal aperture : " << horizAperture << std::endl;
	float fov = 2.0f * std::atan((horizAperture / 2.0f) / focalLength);
	m_Camera.fov = fov;
	std::cout << "\tCamera FOV : " << m_Camera.fov << std::endl;

	// set focal point and aperture
	m_Camera.focalPoint = camera.focusDistance();
	m_Camera.aperture = camera.fStop();
	std::cout << "\tCamera focal point : " << m_Camera.focalPoint << std::endl;
	std::cout << "\tCamera aperture : " << m_Camera.aperture << std::endl;
}

void RenderProcedure::translateLights()
{
	std::cout << "Starting translateLights() procedure" << std::endl;

	// create temporary sky

	Mirage::Vec3 color(0.1f, 0.3f, 0.9f);
	Mirage::Vec3 rotate(1.0f, 1.0f, 1.0f);

	Mirage::Sky sky;
	Mirage::Probe probe;

	sky.horizon = color * 1.0f;
	sky.zenith = 0.5f + rotate * 0.1f;
	sky.probe = probe;
	m_scene.sky = sky;
}

void RenderProcedure::translateGeo()
{
	std::cout << "Starting translateGeo() procedure" << std::endl;
	for (MItDag it; !it.isDone(); it.next())
	{
		MObject obj = it.currentItem();
		if (obj.apiType() == MFn::kMesh)
		{
			MFnMesh mMesh(obj);
			std::cout << "Translating mesh " << mMesh.name().asChar() << std::endl;

			Mirage::Mesh* mesh = new Mirage::Mesh();
			Mirage::Primitive primitive;
			primitive.type = Mirage::eMesh;

			// get transformations
			MFnTransform mXform(obj);

			MVector translation = mXform.getTranslation(MSpace::kWorld);
			primitive.startTransform.p.x = translation.x;
			primitive.startTransform.p.y = translation.y;
			primitive.startTransform.p.z = translation.z;
			std::cout << "\tPrimitive position : " << translation.x << ", " << translation.y << ", " << translation.z << std::endl;

			MQuaternion rotation;
			mXform.getRotation(rotation);
			primitive.startTransform.r.x = rotation.x;
			primitive.startTransform.r.y = rotation.y;
			primitive.startTransform.r.z = rotation.z;
			primitive.startTransform.r.w = rotation.w;
			std::cout << "\tPrimitive rotation : " << rotation.x << ", " << rotation.y << ", " << rotation.z << ", " << rotation.w << std::endl;

			// get vertices
			MPointArray vertices;
			mMesh.getPoints(vertices);
			mesh->vertices.resize(vertices.length());

			for (unsigned int i = 0; i < vertices.length(); i++)
			{
				MPoint point = vertices[i];
				mesh->vertices.emplace_back(point.x, point.y, point.z);
			}

			// get normals
			MFloatVectorArray normals;
			mMesh.getNormals(normals);
			for (unsigned int i = 0; i < normals.length(); ++i)
			{
				mesh->normals.emplace_back(normals[i].x, normals[i].y, normals[i].z);
			}

			// get indices
			MIntArray mTrianglesCount;
			MIntArray mVerticesIndice;
			mMesh.getTriangles(mTrianglesCount, mVerticesIndice);
			for (int polygon = 0; polygon < mMesh.numPolygons(); ++polygon)
			{
				MIntArray vertexList;
				MIntArray faceNormalList;
				mMesh.getPolygonVertices(polygon, vertexList);

				int vertexId[3];
				uint64_t offset = static_cast<uint64_t>(mesh->indices.size());
				mesh->indices.resize(offset + ((uint64_t)mTrianglesCount[polygon] * 3));

				for (int i = 0; i < mTrianglesCount[polygon]; ++i)
				{
					mMesh.getPolygonTriangleVertices(polygon, i, vertexId);
					mesh->indices[offset + (uint64_t)i * 3 + 0] = vertexId[0];
					mesh->indices[offset + (uint64_t)i * 3 + 1] = vertexId[1];
					mesh->indices[offset + (uint64_t)i * 3 + 2] = vertexId[2];
				}
			}

			mesh->rebuildBVH();

			// print more stats
			std::cout << "\tVertices : " << mesh->vertices.size() << std::endl;
			std::cout << "\tNormals : " << mesh->normals.size() << std::endl;
			std::cout << "\tUVs : " << mesh->uvs.size() << std::endl;
			std::cout << "\tIndices : " << mesh->indices.size() << std::endl;
			std::cout << "\tCDF : " << mesh->cdf.size() << std::endl;

			primitive.mesh = Mirage::GeometryFromMesh(mesh);

			// add temporary material
			std::cout << "\tAdding temporary material..." << std::endl;
			Mirage::Material material;
			material.color = Mirage::Vec3(0.4f, 0.6f, 0.2f);
			primitive.material = material;
			
			m_scene.AddPrimitive(primitive);
			m_scene.AddMesh(mesh);
		}
	}    
}

void RenderProcedure::buildScene(MString cameraName)
{
	std::cout << "Starting buildScene() procedure" << std::endl;
	translateCamera(cameraName);
	translateLights();
	translateGeo();
}