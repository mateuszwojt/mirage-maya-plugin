#include <map>
#include <memory>

#include "RenderProcedure.h"
#include "nodes/RenderGlobals.h"

#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MItDag.h>
#include <maya/MFnMesh.h>
#include <maya/MGlobal.h>
#include <maya/MIOStream.h>
#include <maya/MPlugArray.h>
#include <maya/MPlug.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MFnCamera.h>
#include <maya/MRenderView.h>
#include <maya/MFnAreaLight.h>
#include <maya/MFnTransform.h>

#include <utils/Timer.h>
#include <utils/Util.h>

const char *RenderProcedure::name = "MirageRenderProcedure";

void *RenderProcedure::creator()
{
	return new RenderProcedure();
}

MSyntax RenderProcedure::createSyntax()
{
	MSyntax syntax;
	syntax.addFlag("-c", "-camera", MSyntax::kString);
	syntax.addFlag("-w", "-width", MSyntax::kLong);
	syntax.addFlag("-h", "-height", MSyntax::kLong);
	return syntax;
}

MStatus RenderProcedure::doIt(const MArgList &args)
{
	int width = 1;
	int height = 1;
	MString cameraName;
	
	MArgDatabase argsData(syntax(), args);

	if (!argsData.isFlagSet("-width") || !argsData.isFlagSet("-height") || !argsData.isFlagSet("-camera"))
	{
		std::cout << "Wrong number of argument passed to render procedure" << std::endl;
		return MS::kFailure;
	}
	argsData.getFlagArgument("-width", 0, width);
	argsData.getFlagArgument("-height", 0, height);
	argsData.getFlagArgument("-camera", 0, cameraName);

	m_renderOptions = RenderGlobalsNode::getRenderOptions();
	m_renderOptions.width = width;
	m_renderOptions.height = height;
	m_renderOptions.clamp = FLT_MAX;
	m_renderOptions.maxDepth = 4;

	try
	{
		initRender(cameraName);
		preRender();
		render();
		postRender();
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		return MS::kFailure;
	}

	return MS::kSuccess;
}

void RenderProcedure::preRender()
{
	std::cout << "Starting preRender() procedure" << std::endl;

	// build BVH
	m_scene.Build();
	
	// create color buffers
	m_pPixels = new Mirage::Color[m_renderOptions.width * m_renderOptions.height];
	
	// initialize renderer
	m_pRenderer->Init(m_renderOptions.width, m_renderOptions.height);
}

void RenderProcedure::initRender(MString camera)
{
	std::cout << "Starting initRender() procedure" << std::endl;

	m_pRenderer = Mirage::CreateCpuRenderer(&m_scene);

	// build scene
	buildScene(camera);
}

void RenderProcedure::render()
{
	std::cout << "Starting render() procedure" << std::endl;
	std::cout << "Image size: " << m_renderOptions.width << "x" << m_renderOptions.height << ", sampling: " << m_renderOptions.maxSamples << std::endl;
	std::cout << "Rendering mode: " << m_renderOptions.mode << std::endl;

	Mirage::Timer timer;

	MRenderView::startRender(m_renderOptions.width, m_renderOptions.height);
	
	for (int i = 0; i < m_renderOptions.maxSamples; i++)
	{
		std::cout << "Rendering pass " << i << std::endl;
		m_pRenderer->Render(m_Camera, m_renderOptions, m_pPixels);
	}

	std::cout << "Render took " << timer.elapsed() << " milliseconds" << std::endl;

	const uint32_t size = m_renderOptions.width * m_renderOptions.height;
	std::shared_ptr<RV_PIXEL> pixels(new RV_PIXEL[size]);

	for (int y = 0; y < m_renderOptions.height; y++)
	{
		for (int x = 0; x < m_renderOptions.width; x++)
		{
			int i = y * m_renderOptions.width + x;
			Mirage::Color curPixel = m_pPixels[i];
			pixels.get()[i].r = curPixel.x * 255.0f;
			pixels.get()[i].g = curPixel.y * 255.0f;
			pixels.get()[i].b = curPixel.z * 255.0f;
			pixels.get()[i].a = 255.0f;
		}
	}

	MRenderView::updatePixels(0, m_renderOptions.width - 1, 0, m_renderOptions.height - 1, pixels.get());
	MRenderView::refresh(0, m_renderOptions.width - 1, 0, m_renderOptions.height - 1);
	
	MRenderView::endRender();
}

void RenderProcedure::postRender()
{
	std::cout << "Starting postRender() procedure" << std::endl;
}